(function() {

    'use strict';

    angular.module('AppEquestreTV.services', []);
    angular.module('AppEquestreTV.directives', []);
    angular.module('AppEquestreTV.controllers', []);

    angular.module('AppEquestreTV', ['ionic', 'AppEquestreTV.httpInterceptor', 'AppEquestreTV.services', 'AppEquestreTV.directives', 'AppEquestreTV.controllers'])

    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            if (typeof(navigator.notification) == "undefined") {
                navigator.notification = window;
            }
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                StatusBar.styleLightContent();
            }
        });
    })

    .config(function($stateProvider, $urlRouterProvider, $httpProvider, $ionicConfigProvider) {

        $ionicConfigProvider.backButton.text('').icon('ion-ios7-arrow-left');

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'templates/home.html',
                controller: 'homeCtrl as vm'
            })
            .state('sobre', {
                url: '/sobre',
                templateUrl: 'templates/sobre.html',
                controller: 'sobreCtrl as vm',
                resolve: {
                    requestSobre: function($api) {
                        return $api.getSobre();
                    }
                }
            })
            .state('videos', {
                url: '/videos',
                templateUrl: 'templates/videos.html',
                controller: 'videosCtrl as vm'
            })
            .state('contato', {
                url: '/contato',
                templateUrl: 'templates/contato.html',
                controller: 'contatoCtrl as vm'
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/home');
        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('httpInterceptor');
    });

})();