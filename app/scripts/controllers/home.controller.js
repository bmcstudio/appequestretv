(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppEquestreTV.controllers')
		.controller('homeCtrl', homeCtrl);

	// homeCtrl.$inject = [''];

	function homeCtrl() {

		var vm = this;

		vm.openLive = openLive;

		vm.aImages = [	{'src' : 'images/imagem1.jpg' },
						{'src' : 'images/imagem2.jpg' },
						{'src' : 'images/imagem3.jpg' }];


		///////////////////////


		function openLive() {

			// navigator.notification.alert(
			// 	'Não estamos ao vivo no momento.',  // message
			// 	null,         	// callback
			// 	'Atenção',    	// title
			// 	'OK'          	// buttonName
			// );

			if (device)
				switch(device.platform){
					case 'iOS':
						window.open('http://streaming55.sitehosting.com.br:1935/casa/casa/playlist.m3u8', '_system', 'location=no'); break;
					case 'Android':
						window.open('rtsp://streaming55.sitehosting.com.br:1935/casa/casa', '_system', 'location=no'); break;
				}
		}

	}

})();
