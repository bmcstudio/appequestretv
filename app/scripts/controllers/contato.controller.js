(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppEquestreTV.controllers')
		.controller('contatoCtrl', contatoCtrl);

	contatoCtrl.$inject = ['$api'];

	function contatoCtrl($api) {

		var vm = this;
		vm.formcontato = {};

		vm.sendEmail = sendEmail;

		///////////////////////


		function sendEmail() {
			$api.sendEmail(vm.formcontato).then(function(response){
				// console.log(response);
				navigator.notification.alert(
					'E-mail enviado com sucesso.',  // message
					null,         	// callback
					'Obrigado',    	// title
					'OK'          	// buttonName
				);

				vm.formcontato = {};

			}, function(error){
				// console.log(error);
				navigator.notification.alert(
					'Erro ao enviar e-mail.',  // message
					null,         	// callback
					'Atenção',    	// title
					'OK'          	// buttonName
				);
			});
		}

	}

})();
