(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppEquestreTV.controllers')
		.controller('sobreCtrl', sobreCtrl);

	sobreCtrl.$inject = ['requestSobre', '$ionicSlideBoxDelegate'];

	function sobreCtrl(requestSobre, $ionicSlideBoxDelegate) {

		var vm = this;

		vm.sobre = requestSobre.data;

		///////////////////////
	}

})();