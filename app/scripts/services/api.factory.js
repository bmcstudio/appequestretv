(function() {

	'use strict';

	/* jshint validthis: true */

	angular.module('AppEquestreTV.services')
		.factory('$api', api);

	api.$inject = ['$http'];

	function api($http) {

		var urlBase = 'http://www.criarleiloes.com.br/app/',
			api = {
				getSobre  : getSobre,
				getVideos : getVideos,
				sendEmail : sendEmail
			};

		return api;

		///////////////////////////////////////

		function getSobre() {
			return $http.get( urlBase + 'sobre.php' );
		}

		function getVideos(agendaID) {
			return $http.get( urlBase + 'videos.php', { params: { id: agendaID }} );
		}

		function sendEmail(params) {
			return $http({ headers: {'Content-Type': 'application/x-www-form-urlencoded'}, url: urlBase + 'email.php', method: "POST", data: params });
		}
	}

})();